# README
Dorsia is a ~~blog aware~~ static site generator, in Lua. Inspired by [Jekyll](https://github.com/mojombo/jekyll).

Write content with Markdown, templating with Mushache, and bind it all together with some Lua front matter.

## Usage
    luajit --help

## Dependencies
* [LuaJIT](http://luajit.org/)
* [sundown](https://github.com/vmg/sundown)
* [lustache](https://github.com/Olivine-Labs/lustache.git) -- *Note:* Currently needs my [patch](https://github.com/andsve/lustache/commit/e90c4c6ceb430bd9386cf931d23d4b8ad41cc2e2).

## Technical workflow
TODO