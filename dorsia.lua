package.path = "?.lua;lustache/?.lua;" .. package.path

local sundown  = require("sundown/init/init")
local lustache = require("lustache/lustache")
local lfs      = require("lfs")

---------------------------------------------------------------------
-- Application globals
---------------------------------------------------------------------
local binary_name = "luajit dorsia.lua"
local version     = "1.0.0"
local content_filetypes = {
  markdown = "markdown",
  md       = "markdown"
}

local layouts = {}
local conversion_func_lut = {
  markdown = {
    get_path = function( path )
      local out = string.gsub(path, "^(.+)%.(.-)$", "%1.html")
      return out
    end,
    compile = function ( front_matter, file_content )
      local content = sundown.render(file_content)
      return content
    end
  },

  mustache = {
    compile = function ( front_matter, file_content )
      return file_content --lustache:render(file_content, { content = })
    end
  },

  ["nil"] = { -- default action => copy to site output
    get_path = function( path )
      return path
    end,
    compile = function ( front_matter, file_content )
      return file_content
    end
  }
}

---------------------------------------------------------------------
-- Aux
---------------------------------------------------------------------

----------------------------------
-- Output and logging methods
local print_err = function ( ... )
 
  local esc = string.char(27, 91)

  io.write(esc .. '31m')
  print(...)
  io.write(esc .. '0m')
  --print("test")
end

----------------------------------
-- Command line arg methods
local new_arg_option = function ( name, options, value, default, sub_args, description )
  local optn = {
    name        = name,
    options     = options,
    value       = value,
    default     = default,
    sub_args    = sub_args,
    description = description
  }
  return optn
end

----------------------------------
-- Pretty print argument lookup table
local pp_arguments = function ( arg_lut )
  for k,v in pairs(arg_lut) do
    
    local out_str = "  " .. table.concat(v.options, ", ") .. "\t"

    if (v.sub_args and v.sub_args > 0) then
      if (v.sub_args > 1) then

        local sa_list = {}
        for i=1,v.sub_args do
          table.insert(sa_list, "arg" .. tostring(i))
        end
        out_str = out_str .. table.concat(sa_list, ", ")
      else
        out_str = out_str .. "arg" .. "\t"
      end
    else
      out_str = out_str .. "\t"
    end

    if (v.description) then
      out_str = out_str .. "\t" .. tostring(v.description)
    end

    if (v.default) then
      out_str = out_str .. "(default: " .. tostring(v.default) .. ")"
    end

    print(out_str)

  end
end

----------------------------------
-- Parse arguments using argument lookup table
local parse_arguments = function ( arg_lut, arg_list )

  local ret_args  = {}
  local arg       = nil
  local sub_args  = {}
  local sub_count = 0

  -- clear output list with default values
  for _,v in pairs(arg_lut) do
    ret_args[v.name] = v.default
  end

  -- parse all input args
  for i,v in ipairs(arg_list) do

    if (sub_count > 0) then
      table.insert(sub_args, v)
      sub_count = sub_count - 1

      if (sub_count == 0) then
        if (type(arg.value) == "function" ) then
          ret_args[arg.name] = arg.value(sub_args)
        else
          if (#sub_args == 1) then
            sub_args = sub_args[1]
          end
          ret_args[arg.name] = sub_args --arg.value
        end
      end
    else

      -- find correct option, loop argument lut
      local found_opt = false
      for _,a in pairs(arg_lut) do

        -- loop option variations
        for _,o in pairs(a.options) do
          
          if (o == v) then
            arg       = a
            found_opt = true
            break
          end

        end

        if (found_opt) then
          break
        end
      end

      -- see if this is an unknown argument
      if (not found_opt) then
        print_err("Unknown argument!")
        os.exit(-1)
      end

      -- should we collect sub_arguments?
      sub_args = {}
      if (arg.sub_args and arg.sub_args > 0) then
        sub_count = arg.sub_args
      else
        if (type(arg.value) == "function" ) then
          ret_args[arg.name] = arg.value()
        else
          ret_args[arg.name] = arg.value
        end
      end

    end
  end

  return ret_args
end

---------------------------------------------------------------------
-- Data collection methods
---------------------------------------------------------------------

local collect_and_tag = function (directory, dir_filter, file_filter)
  
  local rel_len = #directory + 2
  local dir_stack = { directory }

  -- loop through directory stack
  for _,dir in pairs(dir_stack) do

    -- loop every file entry in current directory
    for file in lfs.dir( dir ) do

      -- try not to get lost..
      if file ~= "." and file ~= ".." then

        local f = dir .. '/' .. file
        local rel_path = string.sub(f, rel_len )
        local attr = lfs.attributes(f)
        --assert (type(attr) == "table")
        
        if attr.mode == "directory" then
          if (dir_filter(f, rel_path, file)) then
            table.insert(dir_stack, f)
          end
        else

          file_filter( f, rel_path, file )

        end

      end
    end

  end

end

local build_input_tree = function ( directory, output )

  local tree = {}

  -- collect content (html, markdown etc)
  collect_and_tag( directory,
      function ( full, rel_path, dir )
        -- ignore hidden unix paths
        if (string.sub(dir, 1, 1) == ".") then
          return false
        end
        if (dir == output) then -- ignore the output dir
          return false
        end
        return true
      end,
      function ( full, rel_path, file )

        local extension = string.match(file, "%.(.+)$")
        local tag = content_filetypes[extension]

        local entry = { }
        entry.path     = full
        

        -- check if dir is a dorsia special dir -> starts with _
        if (string.sub(rel_path, 1, #"_") == "_") then

          if (string.sub(rel_path, 1, #"_layouts") == "_layouts") then
            tag = "mustache"
            entry.layout = true
            rel_path = string.match(rel_path, "(.+)%..+$")
            local layout_name = string.sub(rel_path, #"_layouts" + 2)
            entry.layout_name = layout_name
          end

        else
          entry.out_path = output .. "/" ..
            conversion_func_lut[tostring(tag)].get_path(rel_path)
        end

        entry.rel_path = rel_path
        entry.tag = tag
        table.insert( tree, entry )
        
      end
    )

  return tree
end

----------------------------------
-- Filters tree entries that would
-- generate to the same output files
local filter_tree = function ( tree )
  
  local filtered_tree = {}
  local collision_table = {}
  for _,v in pairs(tree) do
    if (v.out_path) then

      if (collision_table[v.out_path] ~= nil) then
        print_err("Collision of output file '" .. v.out_path .. "':")
        print_err(" Used: " .. collision_table[v.out_path])
        print_err(" Skipped: " .. v.path)
      else
        collision_table[v.out_path] = v.path
        table.insert(filtered_tree, v)
        --print(v.path, v.rel_path, "->", v.out_path)
      end

    else

      -- entry lacks an output file => can't conflict
      table.insert(filtered_tree, v)
    end
  end

  return filtered_tree

end

local parse_front_matter = function ( path, file )
  
  local front_matter_data = {}
  local has_front_matter = false
  local first_line = file:read("*l")
  local front_matter = ""

  -- check and parse front matter
  if (first_line == "---") then

    -- read until next "---" or end of file
    local build_front_matter = true
    repeat
      local line = file:read("*l")

      if (line == nil) then
        build_front_matter = false
      elseif (line == "---") then
        has_front_matter   = true
        build_front_matter = false
      end

      front_matter = front_matter .. line .. "\n"

    until ( not build_front_matter)

  end

  if (has_front_matter) then

    -- parse front matter as Lua
    local front_matter_func, err = loadstring(front_matter,
      "Front Matter for '" .. path .. "'")

    if (front_matter_func == nil) then
      print_err("Could not load Lua front matter for '" ..
        path .. "': " .. err)
    else
      -- add a safe environment to the front matter func
      setfenv(front_matter_func, front_matter_data)
      local s, err = pcall(front_matter_func)
      if (not s) then
        print_err("Error while executing front matter for '"
          .. path .. "': " .. err)
      end

    end

  else
    -- no front matter, reset file cursor
    file:seek("set")
  end

  return front_matter_data
end

local compile_tree = function ( tree )
  
  local compile_list = {}
  local compiled = {}

  -- gather content
  for k,entry in pairs( tree ) do
    
    local f_in, e = io.open(entry.path, "r")
    if (f_in) then

      -- get front matter if it exists
      local front_matter = parse_front_matter(entry.path, f_in)
      local content      = f_in:read("*a")
      f_in:close()

      compile_list[entry.rel_path] = { entry, front_matter, content }
      
    else
      print_err("Could not open '" ..
        tostring(entry.path) .. "': " .. tostring(e))
    end

  end

  compile_entry = function ( k, v )

    local entry = v[1]
    local front_matter = v[2]
    local content = v[3]

    if (entry.compiling) then
      print_err("Cyclic dependency detected!")
      os.exit(-1)
    end
    entry.compiling = true

    local layouts_mustache_lut = {}
    local mt = {
        __index = function ( self, layout )
          
          local layout_path = "_layouts/" .. layout

          if (layouts[layout]) then
            return layouts[layout]
          else

            if (compile_list[layout_path]) then

              if (compiled[layout_path]) then
                return compiled[layout_path].output
              else
                return compile_entry( layout_path, compile_list[layout_path] )
              end

            else
              return "Unknown layout '" .. tostring(layout) .. "'"
            end

          end
        end}
    setmetatable(layouts_mustache_lut, mt)

    if (front_matter.layout) then
      if (not layouts[front_matter.layout]) then

        compile_entry("_layouts/" .. front_matter.layout, compile_list["_layouts/" .. front_matter.layout] )

      end
    end

    -- compile depending on tag type
    local compile_result = conversion_func_lut[tostring(entry.tag)].compile( front_matter, content )--compile_entry( entry, front_matter, content )
    print("Compiling '" .. entry.path .. "'")

    if (front_matter.layout) then

      -- apply layout
      compile_result = lustache:render(layouts[front_matter.layout],
        { content = compile_result,
          page = front_matter
        }, layouts_mustache_lut)
    end

    if (entry.out_path) then

      local f_out, e = io.open(entry.out_path, "w+")
      if (f_out) then
        f_out:write(compile_result)
        f_out:close()

        print("  -> '" .. entry.out_path .. "'")
      else
        
        print_err("Could not open '" ..
          tostring(entry.out_path) .. "': " .. tostring(e))
      end

    end

    entry.output = compile_result

    if (entry.layout) then
      layouts[entry.layout_name] = compile_result
    end
    
    --print(compile_result)
    --print("----")
    compiled[k] = v

    return compile_result

  end


  for k,v in pairs(compile_list) do
    if (not compiled[k]) then

      compile_entry(k, v)

    end
  end

end



---------------------------------------------------------------------
-- Site building methods
---------------------------------------------------------------------
local build_site = function ( directory, output )
  
  -- collect all inputs (layouts, content files & static files)
  local tree = build_input_tree( directory, output )

  -- create output directory
  local mkdir_s, mkdir_e = lfs.mkdir( output )
  if (not mkdir_s) then
    print_err("Error while creating output directory: " ..
      tostring(mkdir_e))
  end

  tree = filter_tree( tree )
  compile_tree( tree )
end




---------------------------------------------------------------------
-- dorsia main
---------------------------------------------------------------------
local dorsia = function (  )
  
  local arg_lut = {
    new_arg_option( "help",      { "-h",      "--help" }, true ),
    new_arg_option( "version",   { "-v",   "--version" }, true ),
    new_arg_option( "directory", { "-d", "--directory" }, true, ".", 1 ),
    new_arg_option( "output",    { "-o",    "--output" }, true, "site", 1 )
  }
  local arg_options = parse_arguments( arg_lut, arg )

  -- show usage help and arguments
  if (arg_options.help) then
    print("usage: " .. binary_name .. " [options]")
    print("Available options are:")
    pp_arguments(arg_lut)
    os.exit(1)
  end

  -- show version 
  if (arg_options.version) then
    print("dorsia " .. tostring(version))
    os.exit(1)
  end

  -- build 
  build_site( arg_options.directory, arg_options.output )

  -- TODO ship with built in webserver, keep track of changed files etc
end



dorsia()